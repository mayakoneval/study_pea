var five = require("johnny-five");
var board = new five.Board();
//setInterval(function () {alert("Hello")}, 3000);
board.on("ready", function() {
	// Create a new `Gyro` hardware instance.
	var gyro = new five.Gyro({
	pins: ["A4", "A5"],
	sensitivity: 0.67
	//sensitivity: 10
	});

	gyro.on("change", function() {
		console.log("X raw: %d rate: %d", this.x, this.rate.x);
		console.log("Y raw: %d rate: %d", this.y, this.rate.y);
		console.log("  x            : ", this.x);
		console.log("  y            : ", this.y);
		console.log("  z            : ", this.z);
		console.log("  pitch        : ", this.pitch);
		console.log("  roll         : ", this.roll);
		console.log("  acceleration : ", this.acceleration);
		console.log("  inclination  : ", this.inclination);
		console.log("  orientation  : ", this.orientation);
		console.log("--------------------------------------");
		console.log(this.pitch.angle);
		console.log(this.roll.angle);
	});
	

	
});