var lifx = require('./lifx');
var util = require('util');
var packet = require('./packet');
var five = require("johnny-five");

var lx = lifx.init();
var board = new five.Board();

//var prevSensor = ;
//var prevBright = 0x028f;

board.on("ready",function(){
	var sensor=new five.Sensor("A0");
	sensor.scale(0,10).on("change",function(){
		console.log(this.value);
		/*
		if(this.value<prevSensor){
			prevBright+=(prevSensor-this.value);
		}
		else {
			prevBright-=(this.value-prevSensor);
		}
		lx.lightsColour(0xd49e, 0xffff,     prevBright,    0x0dac,      0x0513);
		prevSensor = this.value;
		*/

		if (this.value>4.8){
			//off
			lx.lightsOn();
			//lx.lightsColour(0x00, 0x00, 0x028f, 0x0aa, 0x0513);
		}
		else if(this.value>3){
			//slightly on
			lx.lightsOn();
		}
		else if(this.value>2){
			//a bit brighter on
			lx.lightsOff();
		}
		else if(this.value>1){
			//mostly on
			lx.lightsOff();
		}
		else{
			//completely on
			lx.lightsOff();
			
		}
/*
		var led = new j5.Led(13);
		if (this.value<2){
			//LED done
			led.on()
		}
		else{
			led.off()
		}*/
		});
	});
